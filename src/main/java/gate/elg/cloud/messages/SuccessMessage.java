/*
 *    Copyright 2020 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.elg.cloud.messages;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import eu.elg.model.AnnotationObject;
import eu.elg.model.Markup;
import eu.elg.model.responses.TextsResponse;
import io.micronaut.core.annotation.Introspected;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Introspected
@JsonIgnoreProperties(ignoreUnknown = true)
public class SuccessMessage {

  /**
   * Regex to correct in the <em>original</em> content for the Twitter-style HTML escapes that the twitter JSON format
   * applies
   */
  private static final Pattern ESCAPED_CHARS_PATTERN = Pattern.compile("[<>&]");

  /**
   * Regex to correct the <em>escaped</em> content for the Twitter-style HTML escapes that the twitter JSON format
   * applies
   */
  private static final Pattern ESCAPE_SEQUENCES_PATTERN = Pattern.compile("&(?:lt|gt|amp);");

  private String text;

  private Map<String, List<Map<String, Object>>> entities;

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Map<String, List<Map<String, Object>>> getEntities() {
    return entities;
  }

  public void setEntities(Map<String, List<Map<String, Object>>> entities) {
    this.entities = entities;
  }

  /**
   * Turn this response into ELG-compatible annotations with offsets relative to the given original content.
   * @param content the original content that was posted to GATE Cloud
   * @return annotations in the form suitable for an AnnotationsResponse
   */
  public Map<String, List<AnnotationObject>> toElgAnnotations(String content) {
    // GATE Cloud uses the Twitter JSON format, which escapes &, < and > as HTML entities and adjusts the
    // annotation offsets to match.  This means that whenever there is an & in the original content the
    // annotation offsets from GATE Cloud will jump by 4 places more than they should, and whenever there is
    // either of < or > the offsets will jump by 3.  We need to prepare a list of these adjustment points
    // so we can undo the escaping when converting offsets back into ELG-speak.
    TreeMap<Long, Integer> adjustments = new TreeMap<>();

    // first, guarantee there will be a floorEntry for all non-negative offsets
    adjustments.put(0L, 0);

    Matcher m = ESCAPED_CHARS_PATTERN.matcher(content);
    // adjustments are cumulative - each time we pass an escaped char we have to adjust everything
    // to the right of that point
    int lastAdjustment = 0;
    while(m.find()) {
      if(content.charAt(m.start()) == '&') {
        // &amp; skips 4 - GATE Cloud goes from n to n+5 where ELG goes from n to n+1
        lastAdjustment += 4;
      } else {
        // &lt; or &gt; skips 3 (n to n+4 vs n to n+1)
        lastAdjustment += 3;
      }
      adjustments.put((long) (m.end() + lastAdjustment), lastAdjustment);
    }

    return toAnnotations(adjustments);
  }

  @SuppressWarnings("unchecked")
  private Map<String, List<AnnotationObject>> toAnnotations(TreeMap<Long, Integer> adjustments) {
    Map<String, List<AnnotationObject>> elgAnnots = new LinkedHashMap<>();
    if(entities != null) {
      entities.forEach((type, gateAnns) -> {
        elgAnnots.put(type, gateAnns.stream().map(gateAnn -> {
          Map<String, Object> features = new LinkedHashMap<>(gateAnn);
          List<Number> indices = (List<Number>) features.remove("indices");
          return new AnnotationObject()
                  .withStart(adjust(adjustments, indices.get(0).longValue()))
                  .withEnd(adjust(adjustments, indices.get(1).longValue()))
                  .withFeatures(features);
        }).collect(Collectors.toList()));
      });
    }
    return elgAnnots;
  }

  /**
   * Turn this response into a TextsResponse.Text using the text as it was returned from GATE Cloud, but undo the
   * Twitter-style escaping of &amp;, &lt; and &gt;
   * @return a Text with the unescaped string as its content and the entities as annotations.
   */
  public TextsResponse.Text toText() {
    // GATE Cloud uses the Twitter JSON format, which escapes &, < and > as HTML entities and adjusts the
    // annotation offsets to match.  We want to reverse this escaping in the response text and revert the
    // offsets back to where they belong. This means that whenever there is an &amp; in the returned content the
    // annotation offsets from GATE Cloud will jump by 4 places (adding 5 where we want to add 1), and whenever
    // there is either of &lt; or &gt; the offsets will jump by 3.  We need to prepare a list of these adjustment
    // points so we can undo the escaping when converting offsets back into ELG-speak.
    TreeMap<Long, Integer> adjustments = new TreeMap<>();

    // first, guarantee there will be a floorEntry for all non-negative offsets
    adjustments.put(0L, 0);

    Matcher m = ESCAPE_SEQUENCES_PATTERN.matcher(text);
    StringBuilder unescaped = new StringBuilder();
    // adjustments are cumulative - each time we pass an escaped char we have to adjust everything
    // to the right of that point
    int lastAdjustment = 0;
    while(m.find()) {
      String replacement;
      switch(text.charAt(m.start()+1)) { // check the char after the &
        case 'a': // &amp;
          // &amp; skips 4 - GATE Cloud goes from n to n+5 where ELG goes from n to n+1
          lastAdjustment += 4;
          replacement = "&";
          break;

        case 'l': // &lt;
          // &lt; or &gt; skips 3 (n to n+4 vs n to n+1)
          lastAdjustment += 3;
          replacement = "<";
          break;

        default: // &gt;
          lastAdjustment += 3;
          replacement = ">";
      }
      adjustments.put((long) (m.end()), lastAdjustment);
      m.appendReplacement(unescaped, replacement);
    }
    m.appendTail(unescaped);

    Map<String, List<AnnotationObject>> elgAnnots = toAnnotations(adjustments);
    return new TextsResponse.Text().withContent(unescaped.toString()).withMarkup(new Markup().withAnnotations(elgAnnots));
  }

  /**
   * Given a map of adjustments find the last adjustment point before the given index and apply that adjustment in
   * reverse.
   *
   * @param adjustments adjustment map
   * @param index       the index to adjust
   * @return adjusted value
   */
  private static Long adjust(NavigableMap<Long, Integer> adjustments, long index) {
    return index - adjustments.floorEntry(index).getValue();
  }
}
