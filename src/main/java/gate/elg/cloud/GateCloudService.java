/*
 *    Copyright 2020 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.elg.cloud;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Class to represent the definition of a single GATE Cloud service as retrieved from the "/api/shop/services" endpoint.
 */
@Introspected
public class GateCloudService {

  /**
   * Human-readable name of the service.
   */
  private String name;

  /**
   * Quota cost of a single call to this service.
   */
  private int cost;

  /**
   * The URL to call this service.
   */
  private String endpointUrl;

  /**
   * Mapping so that the service knows how to translate bare selectors (without a colon) into full selectors.
   */
  private Map<String, AnnotationSelector> selectorMapping;

  @JsonCreator
  public GateCloudService(@JsonProperty("name") String name, @JsonProperty("shortDescription") String shortDescription,
                          @JsonProperty("cost") int cost, @JsonProperty("endpointUrl") String endpointUrl,
                          @JsonProperty("defaultAnnotations") List<AnnotationSelector> defaultAnnotations,
                          @JsonProperty("additionalAnnotations") List<AnnotationSelector> additionalAnnotations) {
    this.name = name;
    this.cost = cost;
    this.endpointUrl = endpointUrl;
    selectorMapping = new HashMap<>();
    Consumer<AnnotationSelector> handleSelector = (selector) -> {
      selectorMapping.putIfAbsent(selector.getAnnotationType(), selector);
    };
    defaultAnnotations.forEach(handleSelector);
    additionalAnnotations.forEach(handleSelector);
  }


  public String getName() {
    return name;
  }

  public int getCost() {
    return cost;
  }

  public String getEndpointUrl() {
    return endpointUrl;
  }

  /**
   * Map a possibly-unqualified selector expression to the qualified selector that it most closely matches
   * in this service.  If the passed selector is already qualified (contains a colon) then it is returned unchanged.
   * @param selector the selector to qualify
   * @return the qualified value
   */
  public String qualifySelector(String selector) {
    if(selector.indexOf(':') >= 0) {
      // already qualified
      return selector;
    } else {
      AnnotationSelector sel = selectorMapping.get(selector);
      if(sel == null) {
        throw new IllegalArgumentException("Unrecognised annotation selector " + selector);
      } else {
        return sel.toString();
      }
    }
  }

  @Introspected
  public static class AnnotationSelector {

    @JsonCreator
    public AnnotationSelector(String selector) {
      String[] split = selector.split(":", 2);
      if(split.length == 2) {
        annotationSet = split[0];
        annotationType = split[1];
      } else {
        annotationSet = "";
        annotationType = selector;
      }
    }

    private String annotationSet;

    private String annotationType;

    public String getAnnotationSet() {
      return annotationSet;
    }

    public String getAnnotationType() {
      return annotationType;
    }

    public String toString() {
      return (annotationSet == null ? "" : annotationSet) + ":" + (annotationType == null ? "" : annotationType);
    }
  }
}
