package gate.elg.cloud;

import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MutableHttpRequest;
import io.micronaut.http.annotation.Filter;
import io.micronaut.http.filter.ClientFilterChain;
import io.micronaut.http.filter.HttpClientFilter;
import org.reactivestreams.Publisher;

@Filter(Filter.MATCH_ALL_PATTERN)
@Requires(property = "gate.elg.cloud.api-key-id")
public class GateAuthFilter implements HttpClientFilter {

  private CloudBridgeProperties properties;

  public GateAuthFilter(CloudBridgeProperties properties) {
    this.properties = properties;
  }

  @Override
  public Publisher<? extends HttpResponse<?>> doFilter(MutableHttpRequest<?> request, ClientFilterChain chain) {
    return chain.proceed(request.header(HttpHeaders.AUTHORIZATION, properties.basicAuthHeader()));
  }
}
