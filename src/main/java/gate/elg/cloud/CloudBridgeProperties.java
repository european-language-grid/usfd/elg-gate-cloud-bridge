/*
 *    Copyright 2020 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.elg.cloud;

import io.micronaut.context.annotation.ConfigurationProperties;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Base64;

@ConfigurationProperties("gate.elg.cloud")
public class CloudBridgeProperties {

  private URI servicesUrl;

  private String apiKeyId;

  private String apiKeyPassword;

  private Duration minCallInterval;

  private int maxPendingRequests;

  /**
   * The URL to call to retrieve the list of available services.
   * @return the URL this bridge will call at startup
   */
  public URI getServicesUrl() {
    return servicesUrl;
  }

  /**
   * Set the URL this bridge will call to retrieve its list of available cloud services.
   * @param servicesUrl the URL to call, typically <code>&lt;gateCloudBase&gt;/api/shop/services</code>
   */
  public void setServicesUrl(URI servicesUrl) {
    this.servicesUrl = servicesUrl;
  }

  /**
   * The API key used to call GATE Cloud
   * @return the API key ID (username for basic auth)
   */
  public String getApiKeyId() {
    return apiKeyId;
  }

  /**
   * Set the API key that will be used to call both the "services" endpoint and the target services themselves.
   * @param apiKeyId API key ID (the username part)
   */
  public void setApiKeyId(String apiKeyId) {
    this.apiKeyId = apiKeyId;
  }

  /**
   * The API key password used to call GATE Cloud
   * @return the API key password (password part for basic auth)
   */
  public String getApiKeyPassword() {
    return apiKeyPassword;
  }

  /**
   * Set the API key that will be used to call both the "services" endpoint and the target services themselves.
   * @param apiKeyPassword API key password (the password part)
   */
  public void setApiKeyPassword(String apiKeyPassword) {
    this.apiKeyPassword = apiKeyPassword;
  }

  /**
   * Build a basic authentication header from the API key ID and password.
   * @return Basic &lt;base64OfUserColonPassword&gt;
   */
  public String basicAuthHeader() {
    if(apiKeyId != null) {
      return "Basic " + Base64.getEncoder().encodeToString((apiKeyId + ":" + apiKeyPassword).getBytes(StandardCharsets.UTF_8));
    } else {
      return null;
    }
  }

  public Duration getMinCallInterval() {
    return minCallInterval;
  }

  public void setMinCallInterval(Duration minCallInterval) {
    this.minCallInterval = minCallInterval;
  }

  public int getMaxPendingRequests() {
    return maxPendingRequests;
  }

  public void setMaxPendingRequests(int maxPendingRequests) {
    this.maxPendingRequests = maxPendingRequests;
  }
}
