/*
 *    Copyright 2020 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.elg.cloud;

import eu.elg.model.Response;
import jakarta.inject.Singleton;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.BufferOverflowStrategy;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.MonoSink;

import java.time.Duration;
import java.util.function.Consumer;

/**
 * Helper to force a suitable delay between calls to GATE Cloud services so as not to overwhelm the GATE Cloud rate
 * limiter.
 */
@Singleton
public class RateLimiter implements Consumer<MonoSink<Response<?>>>, Subscriber<Long> {

  /**
   * Mono that will always complete empty, but is useful for the side effect of exactly <em>when</em> it completes.
   */
  public final Mono<Response<?>> mono = Mono.create(this);

  /**
   * Ticking clock flux used to provide the rate.
   */
  private Flux<Long> timerFlux;

  /**
   * Circular buffer of waiting subscribers.
   */
  private MonoSink<Response<?>>[] queue;

  private int putIndex = 0;
  private int getIndex = 0;

  private Subscription currentSubscription;

  @SuppressWarnings("unchecked")
  public RateLimiter(CloudBridgeProperties cloudBridgeProperties) {
    queue = new MonoSink[cloudBridgeProperties.getMaxPendingRequests()];

    // this is the key bit that makes the rate limiter work.  The timerFlux is a
    // publisher that emits at the appropriate intervals, and each emission
    // releases *one* of the waiting subscribers on our mono.  If there are
    // no waiting subscribers then we want to disconnect from the ticking flux
    // so that next time a subscriber comes in it can be released immediately
    // rather than waiting for the next tick (e.g. if we're ticking every
    // half a second and there's a 3/4 second delay before the next subscriber
    // comes along, then it can be released immediately rather than waiting for
    // the one second mark.
    timerFlux = Flux.interval(Duration.ZERO, cloudBridgeProperties.getMinCallInterval())
            .onBackpressureBuffer(queue.length, BufferOverflowStrategy.DROP_LATEST);
  }

  /**
   * Called when {@link #mono} is subscribed to.
   *
   * @param sink sink which we will notify of completion at the appropriate time.
   */
  @Override
  public synchronized void accept(MonoSink<Response<?>> sink) {
    if(queue[putIndex] != null) {
      // queue full
      throw new IllegalStateException("Too many concurrent requests");
    }
    queue[putIndex] = sink;
    putIndex = (putIndex + 1) % queue.length;
    if(currentSubscription == null) {
      // no active timer subscription, so start one now
      timerFlux.subscribe(this);
    }
  }

  /**
   * Called when we subscribe to the internal timer flux.  Stores the subscription in a field so we can cancel once the
   * queue is empty.
   *
   * @param s object representing our subscription
   */
  @Override
  public synchronized void onSubscribe(Subscription s) {
    currentSubscription = s;
    s.request(1);
  }

  /**
   * Called for each tick of the "clock".  Releases the next waiting subscriber if there is one, otherwise cancels the
   * timer subscription.
   *
   * @param aLong item emitted from the timer flux.  Value is ignored, all we care about is when it was emitted.
   */
  @Override
  public synchronized void onNext(Long aLong) {
    if(queue[getIndex] != null) {
      queue[getIndex].success();
      queue[getIndex] = null;
      getIndex = (getIndex + 1) % queue.length;
      currentSubscription.request(1);
    } else {
      // queue is now empty
      currentSubscription.cancel();
      currentSubscription = null;
    }
  }

  /**
   * Because of the buffer overflow strategy on the timer flux this should never happen, but if it does we propagate the
   * exception to all waiting subscribers.
   *
   * @param t the throwable that caused the timer flux to error out.
   */
  @Override
  public synchronized void onError(Throwable t) {
    // should never happen, but if it does then propagate
    for(MonoSink<Response<?>> sink : queue) {
      if(sink != null) {
        sink.error(t);
      }
    }
  }

  /**
   * Will never be called, as the timer flux is infinite.
   */
  @Override
  public void onComplete() {
    // will never happen, the flux we're subscribing to is infinite
  }
}
