/*
 *    Copyright 2020 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.elg.cloud;

import eu.elg.ltservice.ELGException;
import eu.elg.ltservice.LTService;
import eu.elg.model.Response;
import eu.elg.model.StandardMessages;
import eu.elg.model.StatusMessage;
import eu.elg.model.requests.TextRequest;
import eu.elg.model.responses.AnnotationsResponse;
import eu.elg.model.responses.TextsResponse;
import gate.elg.cloud.messages.ErrorMessage;
import gate.elg.cloud.messages.SuccessMessage;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.MediaType;
import io.micronaut.http.MutableHttpRequest;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.reactor.http.client.ReactorHttpClient;
import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Main ELG handler class that handles requests for all GATE Cloud service endpoints.  We fetch the list of available
 * services from the GATE Cloud "shop" API endpoint and register one handler for each service in the list.
 */
@Slf4j
@Controller("/process/{slug}")
public class CloudHandler extends LTService<TextRequest, CloudHandler.Context> {

  @Introspected
  @Setter
  @Getter
  public static class Context extends LTService.Context {
    @PathVariable("slug")
    private String slug;
  }

  /**
   * WebClient used to call all the APIs (services list and the actual services).
   */
  private final ReactorHttpClient httpClient;

  /**
   * Configuration from application.yml
   */
  private final CloudBridgeProperties cloudBridgeProperties;

  private final Map<String, GateCloudService> validServices = new ConcurrentHashMap<>();

  private final Mono<Response<?>> rateLimiterMono;

  public CloudHandler(@Client ReactorHttpClient httpClient, CloudBridgeProperties cloudBridgeProperties, RateLimiter rateLimiter) {
    this.httpClient = httpClient;
    this.cloudBridgeProperties = cloudBridgeProperties;
    this.rateLimiterMono = rateLimiter.mono;
  }

  /**
   * Handle a single request for any GATE Cloud service.
   *
   * @param request the request
   * @return a Mono which either completes successfully with the service response once it is returned, or completes with
   * an {@link eu.elg.ltservice.ELGException} encapsulating any errors.
   */
  protected Mono<Response<?>> handle(TextRequest request, Context context) {
    if(validServices.containsKey(context.slug)) {
      GateCloudService service = validServices.get(context.slug);
      log.debug("Sending {} characters to slug {}", request.getContent().length(), context.slug);
      boolean includeText = (request.getParams() != null && request.getParams().get("includeText") != null &&
              !Boolean.FALSE.equals(request.getParams().get("includeText")));
      MutableHttpRequest<String> outgoingRequest = HttpRequest.POST(service.getEndpointUrl(), request.getContent());

      if(request.getParams() != null) {
        Object annotationsParam = request.getParams().get("annotations");
        if(annotationsParam != null) {
          // request contains an "annotations" parameter - translate this into annotation selector query params
          // as understood by GATE Cloud
          String[] annotations = null;
          if(annotationsParam instanceof Collection) {
            annotations = ((Collection<?>) annotationsParam).stream().map(Object::toString).toArray(String[]::new);
          } else {
            annotations = annotationsParam.toString().trim().split("\\s*,\\s*");
          }
          List<CharSequence> finalAnnotations = Stream.of(annotations).map(service::qualifySelector).collect(Collectors.<CharSequence>toList());
          outgoingRequest.getParameters().add("annotations", finalAnnotations);
        }
      }

      outgoingRequest.contentType(Optional.ofNullable(request.getMimeType()).orElse(MediaType.TEXT_PLAIN));

      return rateLimiterMono.switchIfEmpty(
                      httpClient.retrieve(outgoingRequest, Argument.of(SuccessMessage.class), Argument.of(ErrorMessage.class))
                              .last()
                              .map(success -> {
                                if(includeText) {
                                  // includeText - return the text we got back from GATE Cloud
                                  return new TextsResponse().withTexts(success.toText());
                                } else {
                                  // don't includeText - return annotations adjusted to be relative to the original plain text
                                  return new AnnotationsResponse().withAnnotations(success.toElgAnnotations(request.getContent()));
                                }
                              }).onErrorMap(e -> {
                                log.error("Exception during processing", e);
                                return new ELGException(StandardMessages.elgServiceInternalError(
                                        (e instanceof HttpClientResponseException) ?
                                                ((HttpClientResponseException) e).getResponse().getBody(ErrorMessage.class)
                                                        .map(ErrorMessage::getMessage)
                                                        .orElse("Unknown error") : "Unknown error"));
                              }))
              .onErrorMap(IllegalStateException.class, ise -> new ELGException(new StatusMessage().withCode("gate.elg.tooManyRequests").withText("Too many concurrent requests, please try again later")));
    } else {
      return Mono.error(new ELGException(StandardMessages.elgServiceNotFound(context.slug)));
    }
  }


  @PostConstruct
  public void registerHandlers() {
    List<GateCloudService> services = httpClient.retrieve(HttpRequest.GET(cloudBridgeProperties.getServicesUrl()),
                    Argument.listOf(GateCloudService.class)).single().block(Duration.ofSeconds(30));

    if(services != null) {
      log.info("Got {} services from {}", services.size(), cloudBridgeProperties.getServicesUrl());
      for(GateCloudService service : services) {
        // extract the slug from the endpoint URL
        String slug = service.getEndpointUrl().substring(service.getEndpointUrl().lastIndexOf('/') + 1);

        log.info("Registering handler at /process/{} for service {}", slug, service.getName());
        validServices.put(slug, service);
      }
    }
  }
}
