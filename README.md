ELG to GATE Cloud bridge
========================

This is a simple component that bridges between the ELG internal API and the native GATE Cloud document processing API.  It listens for requests according to the ELG API specification and proxies them to calls on the native GATE Cloud API, translating the response back into the ELG expected format.  This provides a quick and easy way to expose all GATE Cloud services on the ELG including those that are only of niche interest - services that receive heavier load will be integrated more tightly to run within the ELG cluster (and take advantage of the auto-scaling capabilities this provides).
